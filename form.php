
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ 
            
            padding-top: 125px;
            width: 500px;
            margin: 0 auto;

            }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Sign Up</h2>
        <p>Please fill this form to create an account.</p>
        <form action="#" method="post">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" id="name" class="form-control">
            </div>  
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" id="email" class="form-control">
            </div>  
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" id="password"class="form-control" required>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="cpassword" id="cpassword" class="form-control">
            </div>
            <div class="form-group">
                <input type="button" class="btn btn-primary" id = "register" value="Register">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>Already have an account? <a href="home.php">Login here</a>.</p>
        </form>
    </div>    
</body>
</html>

<script>
$(document).ready(function() {
$("#register").click(function() {
var name = $("#name").val();
var email = $("#email").val();
var password = $("#password").val();
var cpassword = $("#cpassword").val();
if (name == '' || email == '' || password == '' || cpassword == '') {
alert("Please fill all fields...!!!!!!");
} else if ((password.length) < 8) {
alert("Password should atleast 8 character in length...!!!!!!");
} else if (password != cpassword) {
alert("Your passwords don't match. Try again!");
} else 
{

        
            $.ajax({
                url:'register.php',
                type:'post',
                data:{name1: name,
                email1: email,
                password1: password},
                success:function(response){
                    if(response == 'You have Successfully Registered.....'){
                        alert("You have registered successfully");
                        window.location = "home.php";
                    }else{
                        alert(response);
                    }
                    
                }
            });
        
    
    }
});



/*$.post("register.php", {
name1: name,
email1: email,
password1: password
}, function(data) {
if (data == 'You have Successfully Registered.....') {
$("form")[0].reset();
}
alert(data);
});
}
});
});*/
    })
</script>