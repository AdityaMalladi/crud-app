<?php
require "dbConnect.php";
$name = $address = $salary = $mobile = $email = $gender = $empId =  "" ;
$nameErr = $emailErr = $empIdErr =  "";


if($_SERVER["REQUEST_METHOD"] == "POST"){

    $sql = "SELECT id FROM empDetails WHERE employeeId = ?";        
    if($stmt = $conn->prepare($sql)){
        $stmt->bind_param("i", $param_empId);
        $param_empId = trim($_POST["empId"]);
        if($stmt->execute()){
            $stmt->store_result();
            if($stmt->num_rows > 0){
                $empIdErr = "Employee record already exist.";
            } else{
                $empId = trim($_POST["empId"]);
            }
        } 
        else{
            echo "Wrong with sql";
        }
        $stmt->close();
    }
    
       
    $name = trim($_POST["name"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$name)){
        $nameErr = "Only letters and white space allowed";
    }
    $gender = $_POST["gender"];
    $mobile = trim($_POST["mobile"]);
    $email = trim($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $emailErr = "Invalid email format";
    }
    $address = trim($_POST["address"]);
              
    if(empty($nameErr) && empty($emailErr) && empty($empIdErr)){
        $sql = "INSERT INTO empDetails (name, employeeId, gender,mobile, email, address) VALUES (?, ?, ?, ?, ?, ?)";
        if($stmt = $conn->prepare($sql)){
            $stmt->bind_param("sissss", $name, $empId, $gender, $mobile, $email, $address );
            if($stmt->execute()){
                header("location: home.php");
                exit();
            } 
            else{
                echo "Error in sql";
            }
        }
    $stmt->close();
    }
    
$conn->close();
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style>
        .error {
            color: red;
        }

        form {
                width: 300px;
                margin: 0 auto;
        }
    </style>
</head>
<body>
<a href="home.php" class="btn btn-info btn-lg pull-right">  
          <span class="glyphicon glyphicon-home"></span> Home
</a>
<div class="page-header">
    <h1 align="center">Enter employee details</h1>
</div>
                          
<p><span class="error"></span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Name: <input type="text" name="name" class="form-control" required>
  <span class="error"> <?php echo $nameErr;?></span>
  <br><br>
  Emp Id: <input type="text" name="empId" class="form-control" required>
  <span class="error"> <?php echo $empIdErr;?></span>
  <br><br>
  Gender:
  <label class="radio-inline"><input type="radio" name="gender" value="Male" required>Male</label>
  <label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
  <label class="radio-inline"><input type="radio" name="gender" value="Other">Other</label>
  <br><br>
  Mobile: <input type="tel" name="mobile" class="form-control" required>
  <br><br>
  E-mail: <input type="email" name="email" class="form-control" required>
  <span class="error"> <?php echo $emailErr;?></span>
  <br><br>
  Address: <textarea name="address" rows="5" cols="40"  class="form-control" required></textarea>
  <br><br>
  <input type="hidden" name="id" value="<?php echo $id; ?>"/>
  <input type="submit" name="submit" class="btn btn-primary" value="Submit">  
  <input type="reset" class="btn btn-default" value="Reset">
</form>

</body>
</html>